import React, { Component } from 'react';
import './App.css';
import Description from './Description';
import Header from './Header';
import logo from './logo.svg';

import MovieRow from './redux/MovieRow';

class App extends React.Component {
 

  public render() {
    return (
      <div className="container">
      
        <MovieRow />
      </div>
    );
  }
}


export default App;
