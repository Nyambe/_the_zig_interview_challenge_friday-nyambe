import React from 'react';
import $ from 'jquery';
import MovieContent from './MovieContent';

class MovieRow extends React.Component {
    constructor(props) {
      super(props);

      this.state = {};
   
    this.performSearch("");
    this.getsPopularMoives("");

   // https://api.themoviedb.org/3/movie/popular?api_key=dd44db574821572d93bba59154d41ef5&language=en-US&page=1
    }

    performSearch(search){

    
      const urlString = "https://api.themoviedb.org/3/search/movie?api_key=dd44db574821572d93bba59154d41ef5&language=en-US&page=1&query="+search
        $.ajax(
            {
                url: urlString,
                success: (searchResults) => {
                    console.log("Fetched Date successfully")

                    console.log(searchResults)
                    const results = searchResults.results
                            

                    var movieRows = []



                    results.forEach((movie)=>{

                        movie.Postre_src= "https://image.tmdb.org/t/p/w200"+ movie.poster_path
                        console.log (movie.poster_path)

                        const movieRow = <MovieContent key={movie.id} movie = {movie} />
                        movieRows.push(movieRow)
                    }

                    )

                    this.setState({rows:movieRows})
                },

                error: (xhr,status,err) =>{
                    console.error("Failed to fetch data")
                    
                }
            }
        )


    }






   
    searchChangeHandler(event){
    
            console.log(event.target.value)
           
            const search = event.target.value
            this.performSearch(search)

    }

    getsPopularMoives(){

    
      const urlStringPopular ="https://api.themoviedb.org/3/movie/popular?api_key=dd44db574821572d93bba59154d41ef5&language=en-US&page=1"  
        $.ajax(
            {
                url: urlStringPopular,
                success: (popularMovies) => {
                    console.log("Fetched Date successfully")

                    console.log(popularMovies)
                    const results = popularMovies.results
                            

                    var movieRowspo = []



                    results.forEach((poMovie)=>{

                        poMovie.Postre_src= "https://image.tmdb.org/t/p/w200"+ poMovie.poster_path
                        console.log (poMovie.poster_path)

                        const movieRowpo = <MovieContent key={poMovie.id} movie = {poMovie} />
                        movieRowspo.push(movieRowpo)
                    }

                    )

                    this.setState({rows:movieRowspo})
                },

                error: (xhr,status,err) =>{
                    console.error("Failed to fetch data")
                    
                }
            }
        )


    }
  
  
  
    render() {
      return (
        <div className="container">
             <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <button className="navbar-brand btn-dark" > THE ZIG MOVIE APP</button>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
      
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <button className="nav-link btn-dark" >Home <span className="sr-only">(current)</span></button>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <input className="form-control mr-sm-2" type="search" onChange={this.searchChangeHandler.bind(this)} placeholder="Search" aria-label="Search"/>
            
            </form>
        </div>
      </nav>
      </div>
        {this.state.rows}
        </div>
      );
    }
  }
  export default MovieRow;