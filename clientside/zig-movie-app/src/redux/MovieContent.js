import React from 'react';

class MovieContent extends React.Component {
 

    viewMovie(){

            const url = "https://www.themoviedb.org/movie/" + this.props.movie.id
            window.location.href = url
    }

    render() {
      return (
         
        <div className="container">
            <div className="card"  key= {this.props.movie.id}>
          
        <h2 className="card-header   btn-link"onClick={this.viewMovie.bind(this)}>{this.props.movie.title}</h2>
        <div className="card-body">
        <img className=" mw-100 img-thumbnail rounded float-left" width="200" src={this.props.movie.Postre_src} alt="Postor"/>
            <p className="card-text d-inline-block ">{this.props.movie.overview}</p>
            
            <div className="card-footer bg-transparent border-success">
            <button className="btn btn-primary btn-small" onClick={this.viewMovie.bind(this)}>View</button>
            </div>
        </div>
        </div>

        </div>

            
      );
    }
  }
  
  
  export default MovieContent;